package com.example.antoi.dronemarin;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import static com.google.maps.android.SphericalUtil.*;
import static java.lang.Math.PI;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

/**
 * Created by antoi on 06/06/2018.
 */

public class Waypoint {
    private String nom;
    private LatLng coordonnees;
    private double vitesse;
    private MarkerOptions marker;
    private boolean prendreImage,stationnaire;
    private int index;

    public Waypoint(String nom,MarkerOptions marker,double vitesse,int index){

        this.marker = marker;
        this.nom = nom;
        this.vitesse = vitesse;
        this.coordonnees = this.marker.getPosition();
        this.prendreImage = false;
        this.stationnaire = false;
        this.index = index;
    }

    public String toString()
    {
        update();
        String s = this.nom+"/"+this.coordonnees.latitude+"/"+this.coordonnees.longitude+"/"
                +this.vitesse+"/"+this.prendreImage+"/"+this.stationnaire+"/";
        return s;
    }

    //Calcule l'angle par rapport au Nord entre ce Waypoint et les
    //coordonnées passées
    //Valeur retournées : 0-360
    public double bearingTo(LatLng coord){
        update();
        /*
        double y = (sin(coord.longitude-this.coordonnees.longitude))
                *cos(coord.latitude);
        double x = cos(this.coordonnees.latitude)*sin(coord.latitude)
                - (sin(this.coordonnees.latitude)*cos(coord.latitude)
                *cos(coord.longitude-this.coordonnees.longitude));
        double angleRad = atan2(y,x);
        double angleDeg = -(angleRad * 180 / Math.PI);

        if (angleDeg < 0)
        {
            angleDeg += 360;
        }
        */
        return BearingBetween(this.coordonnees,coord);

    }

    //Calcule l'angle par rapport au Nord entre ce Waypoint et un autre
    public double bearingTo(Waypoint otherWaypoint){
        update();
        return bearingTo(otherWaypoint.coordonnees);
    }

    public double distanceTo(LatLng coord){
        update();
        return distanceBetween(this.coordonnees,coord);
    }

    public double distanceTo(Waypoint otherWaypoint){
        update();
        return distanceTo(otherWaypoint.coordonnees);
    }


    public double BearingBetween(LatLng start, LatLng end){

        /*
        double y = (sin(end.longitude-start.longitude))
                *cos(end.latitude);
        double x = cos(start.latitude)*sin(end.latitude)
                - (sin(start.latitude)*cos(end.latitude)
                *cos(end.longitude-start.longitude));
        double angleRad = atan2(y,x);
        double angleDeg = -(angleRad * 180 / Math.PI);

        if (angleDeg < 0)
        {
            angleDeg += 360;
        }
        */
        return computeHeading(start,end);

    }

    public double BearingBetween(Waypoint startWaypoint, Waypoint endWaypoint)

    {
        return BearingBetween(startWaypoint.coordonnees,endWaypoint.coordonnees);
    }

    public double distanceBetween(LatLng start,LatLng end)
    {
/*
        double R = 6371000.00;
        double  phi1 = start.latitude / 180 * PI ;
        double phi2 = end.latitude / 180 * PI ;
        double deltaPhi = (end.latitude - start.latitude) / 180 * PI;
        double deltaLambda = (end.longitude - start.longitude) / 180 * PI;
        double a = sin(deltaPhi/2) * sin(deltaPhi/2) + cos(phi1) * cos(phi2) * sin(deltaLambda/2) * sin(deltaLambda/2);
        double c = 2 * atan2(sqrt(a), sqrt(1-a));
        double d = R * c ;
        return d;
        */
        return computeDistanceBetween(start,end);

    }

    public double distanceBetween(Waypoint startWaypoint, Waypoint endWaypoint){
        return  distanceBetween(startWaypoint.coordonnees,endWaypoint.coordonnees);
    }






    public void update()
    {
        this.coordonnees = this.marker.getPosition();
    }

}
