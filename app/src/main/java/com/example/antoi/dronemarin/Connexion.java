package com.example.antoi.dronemarin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import java.io.ByteArrayOutputStream;
import java.util.Properties;


public class Connexion extends FragmentActivity {

    private EditText ipTextField,usernameTextField,passwordTextField;
    private TextView etatconnexion;
    private Switch passwordSwitch;
    private boolean savedata = false;
    String Message;
    public PiloteManuel piloteManuel = new PiloteManuel();


    private GestionSocket gestionSocket;
    private Button debug,param;
    private static final String PREFS = "PREFS";
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connexion);

        sharedPreferences = getBaseContext().getSharedPreferences(PREFS, MODE_PRIVATE);

        ipTextField = (EditText) this.findViewById(R.id.editAdIp);
        usernameTextField = (EditText) this.findViewById(R.id.editNomUtil);
        passwordTextField = (EditText) this.findViewById(R.id.editMdp);
        debug = (Button) this.findViewById(R.id.debug);
        param = (Button) this.findViewById(R.id.param);
        passwordSwitch = (Switch) this.findViewById(R.id.switch1);
        etatconnexion = (TextView) this.findViewById(R.id.etatConec);
        Message = "manuel;setangle:90:setspeed:0;";

        ipTextField.setText("192.168.1.25");


        saveData();
        afficheSaveData();
        lanceDebug();
        lanceParam();
    }


    //gestion de la sauvegarde des données si le switch est activé

    public void saveData() {

        passwordSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sharedPreferences
                            .edit()
                            .putString("ip", ipTextField.getText().toString())
                            .putString("user", usernameTextField.getText().toString())
                            .putString("psw", passwordTextField.getText().toString())
                            .apply();
                } else {
                    sharedPreferences
                            .edit()
                            .clear()
                            .apply();
                }
                //System.out.println(isChecked);
                //System.out.println(savedata);
            }
        });
    }

    //On test si des données sont sauvegardées alors on remplit les EditText de connexion avec les données sauvegardées

    public void afficheSaveData() {
        if (sharedPreferences.contains("ip") && sharedPreferences.contains("user") && sharedPreferences.contains("psw")) {
            ipTextField.setText(sharedPreferences.getString("ip", null));
            usernameTextField.setText(sharedPreferences.getString("user", null));
            passwordTextField.setText(sharedPreferences.getString("psw", null));
            passwordSwitch.setChecked(true);
        }
    }


    //Au clic sur le bouton debug on lance l'activité

    public void lanceDebug(){
        debug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Connexion.this,BaseActivity.class));
            }
        });

    }

    //Au clic sur le bouton paramètre on lance l'activité

    public void lanceParam(){
        param.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Connexion.this,Parametres.class));
            }
        });
    }


    //Au clic sur connexion

    @SuppressLint("StaticFieldLeak")
    public void page1 (View view){

        final String Ip = ipTextField.getText().toString();
        final String username = usernameTextField.getText().toString();
        final String psw = passwordTextField.getText().toString();

        if(!Ip.isEmpty() && !username.isEmpty() && !psw.isEmpty()){

        new AsyncTask<Integer, Void, Void>(){
            @Override
            protected Void doInBackground(Integer... params) {

                try {
                    //System.out.println("--------------------TRY---------------------");
                    etatconnexion.setText("connexion à "+Ip);
                    executeSSHcommand(username,psw,Ip);

                    //System.out.println("--------------------FINTRY---------------------");
                } catch (Exception e) {
                    etatconnexion.setText("Déconnecté");
                    e.printStackTrace();
                }

                try {
                    gestionSocket=new GestionSocket();
                    gestionSocket.SendUDP(Message,Ip);
                }

                catch (Exception e){

                }
                return null;
            }
        }.execute(1);



       }
        else
        {
            Toast.makeText(Connexion.this,"Probleme d'info de connexion"
                    ,  Toast.LENGTH_SHORT).show();
        }
    }


    //on essaie de se connecter en SSH pour verifier que la connexion est valide

    public void executeSSHcommand(String user,String password,String host) throws Exception{

    JSch jsch = new JSch();
    Properties prop = new Properties();
    prop.put("StrictHostKeyChecking", "no");
    jsch.setConfig(prop);


    Session session = jsch.getSession(user, host,22);
    session.setPassword(password);



    //session.setTimeout(10000);

    session.connect();

    ChannelExec channel = (ChannelExec) session.openChannel("exec");
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    channel.setOutputStream(baos);
    //channel.setCommand("./client.run setangle:70:setspeed:50;");
    channel.connect();
    channel.disconnect();

        if (session.isConnected()){

            etatconnexion.setText("Connecté");

            //on charge l'activity

            Intent i = new Intent(this, BaseActivity.class);
            i.putExtra("ip",ipTextField.getText().toString());
            startActivity(i);

        }

    }

}
