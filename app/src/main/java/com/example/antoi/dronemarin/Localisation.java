package com.example.antoi.dronemarin;

import android.Manifest;
import android.app.Fragment;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class Localisation extends Fragment {

    MapView mMapView;
    private GoogleMap googleMap;
    private GestionSocket gestionSocket;
    private String str = "";
    private LatLng posDrone = null;
    private MarkerOptions majpos;
    private Marker majdrone;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.localisation, container, false);


        LocationManager locationManager = (LocationManager) getActivity().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        mMapView = (MapView) rootView.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }





        gestionSocket = new GestionSocket();
        new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {
                try {
                    Timer t = new Timer();
                    TimerTask task = new TimerTask() {
                        public void run() {
                            str = gestionSocket.SendAndReceiveUDP("position;cat actualposition.json;","192.168.1.25");

                            posDrone = ft_majPos(str);

                            majpos = new MarkerOptions()
                            .position(posDrone)
                            .title("position du drone");
                        }
                    };

                    t.scheduleAtFixedRate(task,0,1000);

                } catch (Exception e) {
                    System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
                }
                return null;
            }
        }.execute(1);


        ft_CreerMap();

        ft_actualisePos();



        return rootView;
    }



    /*
    //Création de la Map
     */

    public void ft_CreerMap(){
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                //on teste si l'utilisateur a autorisé la localisation alors on affiche sa localilsation

                if((ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED)) {
                    mMap.setMyLocationEnabled(true);

                }

                LatLng larochelle = new LatLng(46.1558, -1.1532);
                if(posDrone == null){
                mMap.addMarker(new MarkerOptions()
                        .position(larochelle)
                        .title("La Rochelle"));
                    mMap.moveCamera(CameraUpdateFactory.newCameraPosition(
                            new CameraPosition.Builder()
                                    .target(larochelle)
                                    .zoom(14.0f)
                                    .tilt(45)
                                    .build()
                    ));

                }
                else {

                    majdrone = mMap.addMarker(majpos);

                    mMap.moveCamera(CameraUpdateFactory.newCameraPosition(
                            new CameraPosition.Builder()
                                    .target(posDrone)
                                    .zoom(14.0f)
                                    .tilt(45)
                                    .build()
                    ));

                }

                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            }
        });
    }



    /*
    // On actualise la position du marqueur toutes les secondes
     */
    public void ft_actualisePos(){


            Timer t = new Timer();
            TimerTask task = new TimerTask() {
                public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (posDrone != null) {
                                majdrone.setPosition(posDrone);
                            }
                        }
                    });

                }
            };
            t.scheduleAtFixedRate(task, 0, 1000);

    }


  /*
  // On analyse le JSONObject et on en extrait la latitude et la longitude
  */

    public LatLng ft_majPos(String data) {

        JSONObject json;
        double lat = 0,longi = 0;
        try {
            json = new JSONObject(data);
            lat = json.getDouble("lat");
            longi = json.getDouble("lon");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new LatLng(lat, longi);

    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}