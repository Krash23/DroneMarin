package com.example.antoi.dronemarin;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import static java.sql.DriverManager.println;

/**
 * Created by antoi on 04/05/2018.
 */

public class GestionSocket {

    public void SendUDP(String Message,String Ipadr) {

        DatagramSocket ds = null;

            try {

                InetAddress serverAddr = InetAddress.getByName(Ipadr);
                ds = new DatagramSocket();
                DatagramPacket dp;


                dp = new DatagramPacket(Message.getBytes(), Message.length(),serverAddr,8000);


                //System.out.println(str);

                ds.setBroadcast(true);
                ds.send(dp);


        } catch (Exception e) {
            System.out.println("-----------------------------------------------------------------------");
            System.out.println(e);
        } finally {
            if (ds != null) {
                ds.close();
            }
        }

    }

    public String SendAndReceiveUDP(String Message,String Ipadr){

        DatagramSocket ds2 = null;
        String str = "";

        try {

            InetAddress serverAddr = InetAddress.getByName(Ipadr);
            ds2 = new DatagramSocket(8000);
            DatagramPacket dp;


            dp = new DatagramPacket(Message.getBytes(), Message.length(),serverAddr,8000);


            //System.out.println(str);

            ds2.setBroadcast(true);
            ds2.send(dp);



            byte[] buffer2 = new byte[8196];

            DatagramPacket packet2 = new DatagramPacket(buffer2, buffer2.length);
            //System.out.println("-----------------------------------------------------------------------");
            ds2.setSoTimeout(3000);
            ds2.receive(packet2);
            //System.out.println("-----------------------------------------------------------------------");
            //System.out.print(" a reçu une réponse du serveur : ");
            str = new String(packet2.getData());
            //System.out.println(str);

        } catch (Exception e) {
            //System.out.println("-----------------------------------------------------------------------");
            System.out.println(e);
        } finally {
            if (ds2 != null) {
                ds2.close();
            }
        }

        return str;
    }


}
