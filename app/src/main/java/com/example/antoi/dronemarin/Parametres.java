package com.example.antoi.dronemarin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by antoi on 09/05/2018.
 */

public class Parametres extends AppCompatActivity {

    private EditText port, vitesse, nbimage, prefix, lien;
    private Button annuler,sauvegarder;
    private static final String PREFS = "PREFS2";
    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.param);

        sharedPreferences = getBaseContext().getSharedPreferences(PREFS, MODE_PRIVATE);

        port = (EditText) this.findViewById(R.id.editport);
        vitesse = (EditText) this.findViewById(R.id.editvitesseauto);
        nbimage = (EditText) this.findViewById(R.id.editnbimgsec);
        prefix = (EditText) this.findViewById(R.id.editPrefixCapture);
        lien = (EditText) this.findViewById(R.id.editFluxMjpg);

        annuler = (Button) this.findViewById(R.id.annuler);
        sauvegarder = (Button) this.findViewById(R.id.sauvegarder);


        ft_annuler();

        ft_sauvegarder();

        afficheSaveData();






    }

    //gestion du bouton annuler

    public void ft_annuler(){

        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Parametres.this.finish();
            }
        });
    }

    //gestion du boutton sauvegarder
    public void ft_sauvegarder(){
        sauvegarder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //on sauvegarde les infos données

                sharedPreferences
                        .edit()
                        .putString("port",port.getText().toString())
                        .putString("vitesse",vitesse.getText().toString())
                        .putString("nbimage",nbimage.getText().toString())
                        .putString("prefix",prefix.getText().toString())
                        .putString("lien",lien.getText().toString())
                        .apply();

                Parametres.this.finish();
            }
        });

    }

    public void afficheSaveData(){

        if(sharedPreferences.contains("port")
                && sharedPreferences.contains("vitesse")
                && sharedPreferences.contains("nbimage")
                && sharedPreferences.contains("prefix")
                && sharedPreferences.contains("lien"))
        {
            port.setText(sharedPreferences.getString("port",null));
            vitesse.setText(sharedPreferences.getString("vitesse",null));
            nbimage.setText(sharedPreferences.getString("nbimage",null));
            prefix.setText(sharedPreferences.getString("prefix",null));
            lien.setText(sharedPreferences.getString("lien",null));
        }
        else {
            port.setText("8000");
            vitesse.setText("3,5");
            nbimage.setText("10");
            prefix.setText("captureBateau");
            lien.setText("http://192.168.1.20/mjpg/video.mjpg");
        }
    }


}
