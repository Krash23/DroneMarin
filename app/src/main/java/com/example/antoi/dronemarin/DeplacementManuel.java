package com.example.antoi.dronemarin;

import android.app.Fragment;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import static java.lang.Double.max;
import static java.lang.Double.min;
import static java.lang.Math.abs;
import static java.lang.Math.asin;
import static java.lang.Math.ceil;
import static java.lang.Math.floor;


public class DeplacementManuel extends Fragment {

    MapView mMapView;
    private TextView affichevitesse, vitesse, inclinaison;
    private GoogleMap googleMap;
    private SeekBar vitesseMax;
    private Button accelerer, freiner, urgence;
    private Connexion connexion = new Connexion();
    private PiloteManuel piloteManuel = connexion.piloteManuel;
    String ip = "";
    private GestionSocket gestionSocket = new GestionSocket();
    private int currentstep = 50;
    private SensorManager sensorManager;
    private Sensor gyroscopeSensor;
    private SensorEventListener gyroscopeEventListener;
    private String videoUrl = "http://192.168.1.20/mjpg/video.mjpg";
    private static final int TIMEOUT = 5;
    private WebView video;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.depmanuel, container, false);

        sensorManager = (SensorManager) getActivity().getApplicationContext().getSystemService(Context.SENSOR_SERVICE);
        gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        inclinaison = (TextView) rootView.findViewById(R.id.inclinaison);
        video = (WebView) rootView.findViewById(R.id.videoView);
        video.setInitialScale(1);
        video.getSettings().setLoadWithOverviewMode(true);
        video.getSettings().setUseWideViewPort(true);
        video.loadUrl(videoUrl);
        mMapView = (MapView) rootView.findViewById(R.id.map2);
        vitesse = (TextView) rootView.findViewById(R.id.vitesseactuelle);
        vitesseMax = (SeekBar) rootView.findViewById(R.id.seekBar);
        affichevitesse = (TextView) rootView.findViewById(R.id.afvitesse);
        accelerer = (Button) rootView.findViewById(R.id.Accelerer);
        freiner = (Button) rootView.findViewById(R.id.Freiner);
        urgence = (Button) rootView.findViewById(R.id.Arreturgence);
        piloteManuel.ip=ip;
        piloteManuel.play();


        ft_GestionSeekBar();

        ft_Accelerer();

        ft_Freiner();

        ft_ArretUrgence();

        //création de la minimap
        mMapView.onCreate(savedInstanceState);
        ft_CreerMiniMap();


        //Gestion du gyroscope pour tourner les moteurs

        ft_GestionGyroscope();


        return rootView;
    }


    //gestion de la seek bar avec un pas de 50 pour limiter la vitesse max du drone

    public void ft_GestionSeekBar(){

        vitesseMax.setMax(1000 / currentstep);

        vitesseMax.setProgress(1000);


        vitesseMax.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 1000;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress * currentstep;
                piloteManuel.setMaxSpeed(progressChangedValue);
                piloteManuel.setMinSpeed(-progressChangedValue);
                affichevitesse.setText("" + progressChangedValue);

            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });
    }


    //accelerer quand on appuie sur le bouton

    public void ft_Accelerer() {
        accelerer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Integer, Void, Void>() {
                    @Override
                    protected Void doInBackground(Integer... params) {
                        try {
                            piloteManuel.startAcceleration();
                            //on affiche la vitesse actuelle du drone
                            vitesse.setText("" + piloteManuel.getSpeed());
                        } catch (Exception e) {

                        }
                        return null;
                    }
                }.execute(1);
            }
        });
    }

    //Freiner quand on appuie sur le bouton

    public void ft_Freiner() {
        freiner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Integer, Void, Void>() {
                    @Override
                    protected Void doInBackground(Integer... params) {
                        try {
                            piloteManuel.startBreak();
                            //on affiche la vitesse actuelle du drone
                            vitesse.setText("" + piloteManuel.getSpeed());
                        } catch (Exception e) {

                        }
                        return null;
                    }
                }.execute(1);
            }
        });
    }

    //Gestion de l'arret d'urgence

    public void ft_ArretUrgence() {
        urgence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Integer, Void, Void>() {
                    @Override
                    protected Void doInBackground(Integer... params) {
                        try {
                            piloteManuel.arretUrgence();
                            //on affiche la vitesse actuelle du drone
                            vitesse.setText("" + piloteManuel.getSpeed());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }.execute(1);
            }
        });
    }

    //Création de la map
    public void ft_CreerMiniMap() {


        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                LatLng larochelle = new LatLng(46.1558, -1.1532);
                mMap.addMarker(new MarkerOptions().position(larochelle).title("La Rochelle"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(larochelle, 14.0f));
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                //on desactive la gestion de la caméra par l'utilisateur
                mMap.getUiSettings().setAllGesturesEnabled(false);
            }
        });
    }


    //Gestion du gyroscope pour tourner les moteurs

    public void ft_GestionGyroscope(){
        gyroscopeEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {

                int angle = calculerAngleTourner(-event.values[0]);
                //System.out.println(angle);
                piloteManuel.setAngle(angle);
                inclinaison.setText("" + angle+"°");
                //System.out.println(event.values[0]);


            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
    }

    //Renvoie un angle entre pilote.minAngle et pilote.maxAngle degrés, par pas de 5 degrés, à partir d'une donnée de l'accéléromètre
    //On gère également le fait que plus le drone va vite moins son angle de rotation peut etre élevé

    public int calculerAngleTourner(float nb) {

        double angleRad = asin(nb);
        double limiteur = abs(this.piloteManuel.getSpeed()) / (this.vitesseMax.getMax());
        double angleDeg = angleRad * 180 / Math.PI;
        angleDeg = angleDeg * 1.5 + 90;


        angleDeg = (min(angleDeg, this.piloteManuel.maxAngle))- 1 * limiteur;
        angleDeg = (max(angleDeg, this.piloteManuel.minAngle))+ 1 * limiteur;

        angleDeg = (max(angleDeg, this.piloteManuel.minAngle))+ 1 * limiteur;
        angleDeg = (min(angleDeg, this.piloteManuel.maxAngle))- 1 * limiteur;


        if (angleDeg > 90) {
            angleDeg = ceil(angleDeg);
        } else {
            angleDeg = floor(angleDeg);
        }

        if (angleDeg % 5 < 2.5 && angleDeg % 5 != 0) {
            angleDeg = angleDeg - (angleDeg % 5);
        } else if (angleDeg % 5 >= 2.5) {
            angleDeg = angleDeg + (5 - angleDeg % 5);
        }

        int result = (int) angleDeg;
        return result;
    }


    public void setIp(String Ip){
        this.ip  = Ip;
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        sensorManager.registerListener(gyroscopeEventListener, gyroscopeSensor, sensorManager.SENSOR_DELAY_FASTEST);

    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        sensorManager.unregisterListener(gyroscopeEventListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}