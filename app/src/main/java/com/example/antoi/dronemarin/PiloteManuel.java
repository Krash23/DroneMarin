package com.example.antoi.dronemarin;

import android.os.AsyncTask;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by antoi on 04/05/2018.
 */

public class PiloteManuel {


    private int currentSpeed = 0;
    private int currentAngle = 90;
    private int lastAngle = 0;
    int minAngle = 30;
    int maxAngle = 150;
    private int minSpeed = -1000;
    private int maxSpeed = 1000;
    private int speedStep = 50;
    private int speedChangeSpeed = 150;
    private int commandsInterval = 150;//toutes les 0.15 secondes
    private boolean lock = false;
    private boolean angleChanged = false;
    private GestionSocket gestionSocket;
    String ip;

    //Methode qui envoie les commandes au socket

    public void sendCommand() {
        if ((this.angleChanged || this.currentSpeed != 0) && !this.lock) {
            this.lock = true; //on verrouille
            final String str = "manuel;setangle:" + this.currentAngle + ":setspeed:" + this.currentSpeed + ";";
            //System.out.println(str);
            new AsyncTask<Integer, Void, Void>(){
                @Override
                protected Void doInBackground(Integer... params) {
                    try {
                        gestionSocket=new GestionSocket();
                        gestionSocket.SendUDP(str,ip);
                    }

                    catch (Exception e){

                    }
                    return null;
                }
            }.execute(1);

            this.lock = false; //on deverouille
            this.angleChanged = false;

        }

    }

    //tant que executé lance la méthode sendCommand() toutes les 0.15 secondes

    public void play() {

        Timer t = new Timer();
        TimerTask task = new TimerTask() {
            int i = 0;
            public void run() {

                sendCommand();

            }
        };

        t.scheduleAtFixedRate(task,0,commandsInterval);
    }


    //augmente la vitesse actuelle de speedStep

    public void startAcceleration()
    {

        setSpeed(this.currentSpeed + this.speedStep);
    }

    //réduit la vitesse actuelle de speedStep

    public void startBreak()
    {
        setSpeed(this.currentSpeed - this.speedStep);
    }

    //bloque la vitesse à 0 et remet l'angle à 90

    public void arretUrgence(){

        setSpeed(0);


    }


    //Affecte la vitesse du bateau. La vitesse doit être comprise entre minSpeed et maxSpeed, ou ces valeurs seront utilisées

    public void setSpeed(int speed){
        if(speed < minSpeed) {
            this.currentSpeed = minSpeed;
        }
        else if (speed > maxSpeed){
            this.currentSpeed = maxSpeed;
        }
        else {
            this.currentSpeed = speed;
        }

    }

    //Affecte l'angle du bateau. L'angle doit être compris entre minAngle et maxAngle, ou ces valeurs seront utilisées

    public void setAngle(int angle){
        this.lastAngle = this.currentAngle;
        if(angle < minAngle){
            this.currentAngle = minAngle;
        }
        if(angle > maxAngle){
            this.currentAngle = maxAngle;
        }
        else {
            this.currentAngle = angle;
        }
        this.angleChanged = ((this.currentAngle != this.lastAngle) || this.angleChanged);
    }

    //Modifie la vitesse max

    public void setMaxSpeed(int speed){

        this.maxSpeed = speed;

    }

    //Modifie la vitesse min

    public void setMinSpeed(int speed){

        this.minSpeed = speed;

    }

    //Récupère la vitesse actuelle

    public int getSpeed(){

        return this.currentSpeed;
    }

}
