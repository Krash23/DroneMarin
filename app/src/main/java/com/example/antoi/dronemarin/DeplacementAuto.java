package com.example.antoi.dronemarin;

import android.Manifest;
import android.app.Fragment;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class DeplacementAuto extends Fragment implements OnMapReadyCallback,GoogleMap.OnMarkerClickListener {

    MapView mMapView;
    private GoogleMap googleMap;
    private Button dessiner, supprimer, close, deleteMarker, envoyer;
    private boolean dessinactif = false;
    private boolean delete = false;
    private int nb = 1;
    private LinearLayout information;
    private TextView nom;
    private String str;
    private GestionSocket gestionSocket;
    ArrayList<LatLng> points;
    ArrayList<Polyline> listePolyline;
    ArrayList<Waypoint> listeMarker;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.depauto, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapauto);
        mMapView.onCreate(savedInstanceState);
        close = (Button) rootView.findViewById(R.id.fermer);
        nom = (TextView) rootView.findViewById(R.id.nomMarker);
        deleteMarker = (Button) rootView.findViewById(R.id.delMarker);
        information = (LinearLayout) rootView.findViewById(R.id.info);
        mMapView.onResume(); // needed to get the map to display immediately
        points = new ArrayList<>();
        listePolyline = new ArrayList<>();
        listeMarker = new ArrayList<>();
        dessiner = (Button) rootView.findViewById(R.id.dessiner);
        supprimer = (Button) rootView.findViewById(R.id.supprimer);
        envoyer = (Button) rootView.findViewById(R.id.Envoyer);
        str="Waypoints;";

        gestionSocket=new GestionSocket();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        GestionDessiner();

        GestionSupprimer();

        GestionEnvoyer();


        GestionClose();




        return rootView;
    }

    private void GestionEnvoyer() {
        envoyer.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listeMarker.size() >= 1  ){
                    for (int i=0; i< listeMarker.size();i++){
                        str += listeMarker.get(i).toString();
                        if(i != listeMarker.size() -1 ){
                            str += listeMarker.get(i).bearingTo(listeMarker.get(i+1))+"/";
                            str += listeMarker.get(i).distanceTo(listeMarker.get(i+1))+"/\n";

                        }
                    }
                    str += ";NO";
                    System.out.println("-------------------------------------------------------");
                    System.out.println(str);
                    System.out.println("-------------------------------------------------------");
                    new AsyncTask<Integer, Void, Void>() {
                        @Override
                        protected Void doInBackground(Integer... params) {
                            try {
                                gestionSocket.SendUDP(str,"192.168.1.25");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    }.execute(1);


                }

            }
        }));

    }

    /*
    //Au clic du bouton dessiner on active le dessin de marqueur sur la map
     */
    public void GestionDessiner(){
        dessiner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dessinactif = !dessinactif;
                if (dessinactif) {
                    dessiner.setText("Stop");
                } else {
                    dessiner.setText("Dessiner");
                }

            }
        });
    }

    /*
    //Au clic du bouton supprimer on supprime le marqueur
     */

    public void GestionSupprimer(){

        supprimer.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                points.clear();
                delete = true;
                mMapView.getMapAsync(DeplacementAuto.this);
                nb=1;
                information.setVisibility(View.INVISIBLE);
            }

        });
    }

    /*
    //Au clic du bouton close on ferme la fenetre d'info du marqueur
     */

    public void GestionClose(){
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                information.setVisibility(View.INVISIBLE);
            }
        });
    }


    @Override
    public void onMapReady(final GoogleMap mMap) {
        googleMap = mMap;
        LatLng larochelle = new LatLng(46.1558, -1.1532);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(larochelle, 14.0f));

        if ((ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)) {
            mMap.setMyLocationEnabled(true);

        }

        if (delete) {
            mMap.clear();
            delete = false;
        }


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                if (dessinactif) {

                    final MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(point);
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    markerOptions.title("Point n° " + nb);


                    // Instantiating the class PolylineOptions to plot polyline in the map
                    PolylineOptions polylineOptions = new PolylineOptions();

                    // Setting the color of the polyline
                    polylineOptions.color(Color.GREEN);

                    // Setting the width of the polyline
                    polylineOptions.width(12);

                    // Adding the taped point to the ArrayList
                    points.add(point);

                    //ajouter un Waypoint à l'arraylist
                    listeMarker.add(new Waypoint("Waypoint"+(listeMarker.size()+1),markerOptions,3,listeMarker.size()));

                    // Setting points of polyline
                    polylineOptions.addAll(points);

                    // Adding the polyline to the map
                    Polyline line = mMap.addPolyline(polylineOptions);
                    listePolyline.add(line);

                    // Adding the first marker to the map
                    mMap.addMarker(markerOptions);

                    nb++;

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 14));




                }
            }
        });


        mMap.setOnMarkerClickListener(this);


    }

    /*
    //gestion du clic sur les marqueurs
     */


    public boolean onMarkerClick(final Marker marker) {

        information.setVisibility(View.VISIBLE);
        nom.setText("Propriété de : "+marker.getTitle());
        deleteMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marker.remove();
                for(int i=0;i<points.size();i++) {
                    if (marker.getPosition().equals(points.get(i))) {
                        points.remove(i);
                        listeMarker.remove(i);
                        nb--;
                        clearTracer();
                        tracerRoute();

                    }
                }
                information.setVisibility(View.INVISIBLE);

            }



        });

        return false;
    }

    /*
    //Permet de tracer les chemins entre les marqueurs
     */

    public void tracerRoute(){

        // Instantiating the class PolylineOptions to plot polyline in the map
        PolylineOptions polylineOptions = new PolylineOptions();

        // Setting the color of the polyline
        polylineOptions.color(Color.GREEN);

        // Setting the width of the polyline
        polylineOptions.width(12);

        polylineOptions.addAll(points);

        Polyline line = googleMap.addPolyline(polylineOptions);
        listePolyline.add(line);

    }

    /*
    //Permet de netoyer le chemin tracé
     */

    private void clearTracer(){
        for(Polyline line : listePolyline){
            line.remove();
        }
        listePolyline.clear();
    }







    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}